syntax on
set number      "line numbering"
set incsearch   "search as you type"
set ignorecase  "case-insensitive search"
set smartcase   "case-sensitive search if the search term contains any capital letters"
set autoindent  "I thought this was the default, but whatever. Make SURE this is set"
set spell       "Spell-checking"
set expandtab   "convert tabs to spaces"
set smarttab    "insert spaces instead of a tab char when tab is pressed"
set tabstop=2
set scrolloff=3 "make sure 3 lines are always shown above or below the cursor"
set mouse=a     "use mouse for scrolling and cursor position"

packloadall
