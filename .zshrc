
source .zsh/antigen/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle sudo
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search

antigen theme bureau

antigen apply

if test "`ls -A $HOME/.zsh`"; then
  for file in $HOME/.zsh/**; do
    source $file
  done
fi

export EDITOR=vim VISUAL=vim

if which startx; then
  aliases[clip]='xclip -selection c'
  aliases[vsc]='vscodium .'
else
  echo 'x11 not installed, not setting xserver-dependent aliases' >> /tmp/zshrc.log
fi


